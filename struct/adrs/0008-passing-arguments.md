# 1. Avoid duplicate information in argument lists

Date: 2022-01-25

## Status

Accepted

## Context

This kind of things appeared in the code:

```kotlin
ProductBuilder(
    override val productId: ProductId,
    override val productDTO: ProductDTO,
)
```

A productDTO contains a productId. So we're passing the same information twice. We could even get inconsistent information where we get a product ID 'p1' in the first argument while the product DTO in the second argument contains product ID 'p2'.

So it's preferable to avoid this. Because it's extra information/work that is not needed and because it can be the cause of bugs and confusion.

## Decision

Only pass the productDTO as argument. Add  `val productId = productDTO.productId` to minimize the impact for dependencies as a migration path or if the `productId` field is very useful to have.

## Consequences

-
