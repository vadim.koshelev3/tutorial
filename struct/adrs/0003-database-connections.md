# 3. Database Connections

Date: 2021-04-14

## Status

Accepted

## Context

At the moment all database connections are going through a single admin user, which isn't secure and also takes away the reserved slots for admin users.

We have different types of users:

### System users

Users being used by applications.

### Read-only users

Human read-only access to the database. Mainly for non-technical people.

### Personal users

Human read-write access to the database. Mainly for technical people.

The different options for personal accounts are:
* Having one global readwrite account
* One readwrite account per team
* AWS IAM integration
* Managing a single database account per user through terraform
## Decision

### System users

We will create 3 roles for applications:

* K8S (SELECT, INSERT, UPDATE, DELETE)
* Lambda (SELECT, INSERT, UPDATE, DELETE, TEMPORARY)
* Liquibase (ALL)

### Read only users

* One read-only user per environment

### Read write users

* Enable IAM authentication on all environment that will have to fetch a token for accessing the database (temporary credentials). All users with the poweruser or account-admin privileges should be able to login into the databases.
* Create an additional read-write user for DEV, SIT and LOAD-TEST

## Consequences

* Testing is needed for validation of the new privileges.
* Applications need to receive the new credentials.
* Additional procedures are necessary to explain how to fetch your IAM token for RDS.
* Coding in terraform to apply this configuration.
