# 2. Runbooks and playbooks

Date: 2021-03-17

## Status

Accepted

## Context

To support the P&A services in production we need to document failure scenarios and how to solve them using runbooks and playbooks. 

They need to be accessible and editable for the development teams within P&A, and should also be accessible in a read-only mode for other teams within TUI.

They need to be searchable

Ideally they are versioned and follow the deployment of the application into the different environments

## Decision

We will create a new git repository to hold the runbooks and playbooks. This could be revisited once we have version streams implemented.

We will use markdown as the documentation format

## Consequences

Runbooks and playbooks will not get lost in confluence, but have their own repository, allowing for each local searching.

Versioning is core to git, any changes to the runbooks and playbooks will be in a new commit

Sharing with non-technical teams should still be ok, as markdown is supported in gitlab

Providing access means people will need to get access to the runbook and playbook repository