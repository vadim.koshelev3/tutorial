# 4. Command app code structure

Date: 2021-05-17

## Status

Proposal

## Context

We're working with many devs at the same time on the code, but there's no consistency in the code structure, making it difficult to:
* find code
* decide where new code should be placed

### src/main structure

* /config: Contains all SpringBoot configuration files.
* /domain: Contains all domain specific code
  * /linking/acco: All domain code that works across acco aggregates goes here
  * /linking/flight: All domain code that works across flight aggregates goes here
  * /linking/transfer: All domain code that works across transfer aggregates goes here
  * /<example aggregate root> 
    * /aggregates: contains the aggregate definitions
    * /commands: contains the commands, typically sent from BusinessEventHandler to CommandHandler
    * /events: contains domainevents (internal within command app). Naming: *DomainEvent
    * /handlers: contains domaineventhandlers, commandhandlers, businesseventhandlers
    * /vo: contains value objects specific to this aggregateroot
    * /listeners: contains the sqs event listeners
    * /converters: contains the converters used by the businesseventhandlers to convert to the correct vo's and commands
  * /framework: contains the framework for domain code. Implementations are in infrastructure.
  * /vo: contains value objects that are used across multiple aggregate roots
* /integration
  * /events: contains integrationevents sent to external applications (e.g. sqs). Naming: *IntegrationEvent 
* /infrastructure: contains solution specific infrastructure implementation 

### src/test structure
* The structure should follow the structure of src/main, and test class names should be formatted as `classNameTest` for unit tests, `R2dbc*Test` for database tests, `*IntegrationTest` for other integration tests. 
  to allow easy finding of tests using `ctrl+shift+t` 
* resources events directory should have a flat structure, grouping by aggregate type and version
  * discountrule/0.11/...
  * yieldbarring/0.7/...
  * yieldinvalidation/0.6/...
  * accommodationpriceset/0.10/...
  * accommodationpriceset/0.11/...
  * flight/0.7/...
  * inventory/6/...
  * inventory/7/...
  * products/0.8.3/...
  * products/0.9.0/...
  * flightcomponent/0.5.2/...
  * flightunit/0.5.1/...
  * etc...
  * One special directory exists within events: lifecycle contains events for different aggregates, specifically designed to test lifecycle

### Use of common-model vo
Using common.model.vo value objects is more confusing than creating new vo's in cmd app.
Due to the often needed dependency on jackson (@get annotation), vo's were being duplicated, which is not good.

## Consequences

* It will be easier to have an overview of existing code
* It will be easier for new developers to find examples of how classes are interconnected 
