# 5. Query app code structure

Date: 2021-05-11

## Status

Proposal

## Context

We're working with many devs at the same time on the code, but there's no consistency in the code structure, making it
difficult to:

* find code
* decide where new code should be placed

## Query app code structure changes

### Cleanup

* Rename package com.tui.engines.price.availability.app to com.tui.engines.price.availability.query.app  
  -> When importing, IntelliJ sometimes gives the choice between cmd.app and query.app. It will be easier to decide.
* Rename the `application/price-availability` folder to `application/query`. Also look for usages in ci/other scripts.
* /config: Clean up tracing config after open-telemetry tracing is set up.
  /config/serializer: should move to /infrastructure/serializer
* QueryProperties.kt: Move to /config
* Run dead code analysis
* Move package builder from usecase to domain
* Move deposit code from usecase to domain
* Move price code from usecase to domain
* Logger implementations like Sfl4jBuffer should be in /infra/log

### src/main structure

* /config: All SpringBoot configuration files. This means `*Configuration` files and `*Properties` files.
* /controller: REST controllers. Classes should be named `*RestController`
* /controller/validation: REST request validation code. Validators should be named `*Validator`
* /domain
  * /<example> - collects some typical sub-packages that will be required for each of the entries below
    * /repository: repositories to support the logic
    * /api: Api for this logic (interface, request/response objects)
    * /vo: value objects only to be used by this logic
  * /price: price logic
    * /flight: flight pricing
    * /accommodation/{data,log,discount,base,supplement...}: accommodation pricing (subpackages for discount, base price, supplements...)
    * /yield/{flight,accommodation,pkg}: yielding
    * Also includes repositories in the correct packages! Repository implementations belong in /infra/repository/...
  * /pkg: package builder logic
    * /accommodation: acco repository, stay builder, multi-room allocator...
    * /builder: package builder interface and implementation(s)
    * /flight: flight repository
  * /vo: value objects shared over many domains (ids...)
* /infra(structure): contains solution specific infrastructure implementation
* /infra/repository/query: query repository implementations (read-only)
* /infra/repository/cache: cache-related repository implementations
* /infra/repository/update: updating repository implementations
* /infra/tracer: tracer implementation(s)
* /repository/cache: cache-related repositories
* /repository/update: updating repositories
* /usecase/pkg: Package use case interfaces and their implementations in `impl`
* /usecase/stay: Stay use case interfaces and their implementations in `impl`

## Consequences

* It will be easier to have an overview of existing code
* It will be easier for new developers to find examples of how classes are interconnected 
