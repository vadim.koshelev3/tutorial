# 9. TA guidelines

Date: 2022-02-10

## Status

Accepted

## Context

To make sure the technical analysis (TA) happens thoroughly, allowing both a *good estimation*
and *smooth development* of the feature(s) we want to document some guidelines that identify
a good technical analysis. This can serve both as a kind of checklist for whoever is doing
the technical analysis. Or as a way for the developers to indicate that a TA is not ready
to be estimated and needs more work in one or more area's, needs to be more refined.

We have also noticed that, especially when working on 'serverless' code, the AWS setup that
comes with it can be a source of problems when finalizing the system. Some of these guidelines
push the validation of the AWS setup forward into the timeline, to the very beginning. This
allows the team to debug the AWS setup when the code is still extremely simple. The configuration
problems can be identified very quickly that way.

## Decision

### General Principles

We generally aim to:
 * integrate early rather than late
 * document the dataflow both visually using a diagram AND the data-structures/interfaces on
   each integration point.
 * immediately support tracing
 * immediately provide debug logging
 * immediately involve automation QA's to ensure testability

### Goal

Every TA document should have a few sentences explaining the purpose of the changes being explained. Why are
we doing this? What is it for? What's the benefit? This allows everybody to understand the value being added. Which
is why we do it in the first place.

### Document

The TA document should be written on Confluence. It should link to the JIRA epic and to all
functional analysis (if any) related to the TA. It should contain a diagram explaining the data
flow. That diagram should contain:
 * All different 'nodes' in the data flow (queues, lambdas, apps, beans, APIs, databases)
 * The names of all those resources
 * The connections between those nodes

The TA document should specify the types and formats of all the events and other messages used to communicate
between the nodes. It should use the 'cloud events' standard for these messages as much as possible.

### Skeleton

#### Serverless

The first story (or stories) for a 'serverless' epic should be a 'skeleton' that:
 * Sets up the lambdas in terraform
 * Set up the SQS queues in terraform
 * Set up other terraform resources
 * Implement basic lambda's that:
   * Consume the incoming event/parameter and log it on debug level
   * Access all 'infrastructure' the actual code will need (this validates AWS permissions, 
     an example is doing an extremely simple query in the DB just to validate the database 
     can be reached and the credentials are correct)
   * Produce a valid result and log it on debug level
  * The Skeleton should be as simple as possible, but not simpler.

The goals of the skeleton are:

 * Validate the AWS configuration: data can flow through the system
 * Logging: It's easy to follow the data flowing through the system in DataDog. A common prefix can help here.
  * logger.debug(`Offer-store: export for product $productId: starting`)
  * logger.debug(`Offer-store: export for product $productId: done`)
 * Define and validate all interfaces
 * Get from the start of the data flow to the end.
   * If there are multiple starts and/or multiple ends, choose 1 of each. Choose the ones that are easy to do.
 * Tracing: Integrate with the DataDog tracing.


#### Apps (kotlin)

The first story (or stories) should be a 'skeleton' that:
* Sets up the beans by creating them and adding them to a `@Configuration`. Some examples:
  * Create the Repositories
  * Create the UseCases
  * Create the REST endpoints
  * Create the Services
* Creates all DTOs and VOs used to communicate between those beans
* Implement beans in the simplest way possible, allowing for end-to-end communication.
* Every bean should:
  * Consume the incoming event/parameter and log it on debug level
  * Access all 'infrastructure' the actual code will need (this validates AWS permissions,
    an example is doing an extremely simple query in the DB just to validate the database
    can be reached and the credentials are correct)
  * Produce a valid result and log it on debug level
* In case a new API is required, or a breaking change to an API is required:
  * Make sure to cover the static API tests (the ones running from datadog)



## Consequences

 * Estimations should become easier and more precise.
 * There should be less confusion when the implementation needs to happen.
 * TA should be more 'complete'. Fewer facets of the TA should be forgotten.
 * TA might become more work, though...
 * It's easier to pinpoint problems in the data flow.
 * Multiple developers can work on the same feature(s) without (a lot of) conflicts.
