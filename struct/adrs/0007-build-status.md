# 7. Build Status

Date: 2021-09-09

## Status

Proposed

## Context

We strive for code that has good structure, is well-tested and is maintainable. We also want our
pipeline to be in a deployable state at all times. 

To monitor the code quality we integrated Sonar into our pipeline. On every Merge Request Sonar
checks the quality of the added code and provided us with a report. We should use that report
to improve the quality of our code.

Also, our pipeline is our way to deploy to dev, sit, pre-prod, load-test and prod. So if we
'break' the pipeline (meaning it has status `Failed`), we break our ability to deploy. This
is not a situation we want to be in, since we strive to by continuously integrating and deploying.

## Decision

We agreed with the developers that we impose trust on them in that they will follow these steps
as to avoid having to introduce a blocking Sonar Quality gate and to keep our pipeline in a
deployable state:

### Merge Requests

1. If you create a MR, check the Sonar results
2. Make sure the Sonar results (quality gate) is green, and if not explain in the comments why
3. Ask for a review, all code should have 2 people involved: either 1 author and 1 reviewer or 2 authors.
   In the case of 2 authors, said authors bear the responsibility for their own and the other author's
   code.
5. If the reviewer agrees with you arguments in case the quality gate is not green, the MR can be merged.
6. You add an item on the agenda of the next tech talk to talk about the Sonar rule that marked 
   the Quality Gate as closed

### Master Branch

After a merge request is merged, the merger is responsible for monitoring the master branch build
is successful, including, but not limited to, the deployment to dev, the functional tests (the ones
required for pipeline success) and the Apigee spec generation. If the master build fails, the merger
is required to either fix the problem within 2 working hours or to roll back the merged changes.

## Consequences

While we will not have a blocking quality gate we will monitor the quality of our contributions
and will keep the master branch in a deployable state.
