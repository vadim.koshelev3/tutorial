# 11. RDS Proxy

Date: 2022-05-11

## Status

Accepted

## Context

We have two ways to connect to the RDS database:
1. Direct RDS connection
2. Through RDS Proxy

A standard is needed in order to have a clear view about which type of connection we are using for which purpose.

### RDS Proxy advantages
* Load balancing (for read-only connections)
* Can help applications in case of RDS scaling to ensure new nodes are added/removed
* Can limit the amount of open database connections towards the database

### RDS Disadvantages
* Not all PostgreSQL parameters can be applied/used (like TCP Keepalive). Example incident: [OT-26978](https://jira.tuigroup.com/browse/OT-26978)
* RDS Proxy is not configurable
* Extra hop in the chain

## Decision

As we have a limit of 5.000 connections towards our database and only 1.000 concurrent lambda invocations possible on our account as well as a connection pool for the kubernetes applications. Using RDS Proxy doesn't bring us additional advantages for usage for write actions as we only have 1 writer instance anyway and no need for load balancing. Read-only actions for sure have a bigger advantage of using the RDS Proxy as it will ensure load balancing between all reader instances.

* Write only actions: Direct connection to the database
* Read-only actions: RDS Proxy

## Consequences

* Two types of connections need to be made in case of mixed usage (one for read-only and one for read-write)
