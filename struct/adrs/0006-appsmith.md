# 6. AppSmith

Date: 2021-09-09

## Status

Accepted

## Context

PO's and BA's want to investigate the status of our (query) data because of incoming requests for 
information from other teams. They can now access the database, but this is a very low-level view
of the data. And it does not allow for some 'smart' integration. You need extra knowledge to link
everything together.

It would be nice if these people could use a simple UI tool to check the status of our query data.
For example: why is acco A123456 now showing up in responses?

A low-code UI design tool seems like a good fit here. 

## Decision

We're introducing [AppSmith](https://github.com/appsmithorg/appsmith) as a way to provide UI tools to
PO's, BA's... AppSmith is Open Source and we can run on our own servers.

From now on, all tooling we create that requires a UI *will* use AppSmith unless otherwise agreed upon.


## Consequences

DevOps team will set up our own AppSmith instance on AWS and we can start creating simple apps with it.
