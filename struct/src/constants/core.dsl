//Service Constats
!constant GROUP_PANDA_WHOLESALE_SERVICE "PandA Wholesale Service"

// Product Service

// TAGS
!constant TAG_EXTERNAL_SERVICE "External Service"
!constant TAG_RELATIONSHIP_USES "Relationship: Uses"
!constant TAG_RELATIONSHIP_DATA_FLOW "Relationship: Data flow"
!constant TAG_RELATIONSHIP_DATA_FLOW_SYNC "Relationship: Data flow Sync"
!constant TAG_RELATIONSHIP_DATA_FLOW_ASYNC "Relationship: Data flow Async"
!constant TAG_DATA_FLOW "Relationship: Data flow"
!constant TAG_DOMAIN "Domain"


//Event Constats
!constant YIELD_FLIGHT_ACCOMMODATION_FLIGHT "Yield Flight Acccommodation Flight"
!constant ACCOM_PRICE_SET_CREATED "Accom Price Set Created"
!constant ACCOM_PRICE_SET_CHANGED "Accom Price Set Changed"
!constant TRANSPORT_PRICE_SET_CREATED "Transport Price Set Created"
!constant TRANSPORT_PRICE_SET_CHANGED "Transport Price Set Changed"
!constant INVENTORY_TRANSACTION_CREATED "Inventory Transaction Created"

//Event Tags
!constant TAG_YIELD_FLIGHT_ACCOMMODATION_FLIGHT "Event: Yield Flight Acccommodation Flight"
!constant TAG_ACCOM_PRICE_SET_CREATED  "Event: Accom Price Set Created"
!constant TAG_ACCOM_PRICE_SET_CHANGED  "Event: Accom Price Set Changed"
!constant TAG_TRANSPORT_PRICE_SET_CREATED  "Event: Transport Price Set Created"
!constant TAG_TRANSPORT_PRICE_SET_CHANGED  "Event: Transport Price Set Changed"
!constant TAG_INVENTORY_TRANSACTION_CREATED_EVENT  ".Event: Inventory Transaction Created"
!constant TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT  ".Event: Inventory Transaction Created Split"
!constant TAG_JOURNEY_INVENTORY_CHANGE_EVENT  ".Event: Journey Inventory Change"



!constant TAG_INVENTORY_TRANSACTION_CREATED_SPLITT  "Inventory Transaction Created Split: Event"
!constant TAG_INVENTORY_TRANSACTION_CREATED_COMMAND  "Inventory Transaction Created: Command"
!constant TAG_INVENTORY_TRANSACTION_CREATED_DOMAIN_EVENT  "Inventory Transaction Created: Domain Event"

//Value Stream Tags
!constant TAG_OFFERS_VALUE_STREAM "Value Stream: Offers"

// AWS ICONS

!constant ICON_AWS_API_GATEWAY "Amazon Web Services - Amazon API Gateway"
!constant ICON_AWS_RDS "Amazon Web Services - RDS Multi AZ"
!constant ICON_AWS_SQS "Amazon Web Services - Simple Queue Service"
!constant ICON_AWS_SNS "Amazon Web Services - Simple Notification Service"
!constant ICON_AWS_ELASTIC_CACHE "Amazon Web Services - ElastiCache"
!constant ICON_AWS_KINESIS "Amazon Web Services - Kinesis"
!constant ICON_AWS_LAMBDA "Amazon Web Services - Lambda"
!constant ICON_AWS_EVENT_BRIDGE "Amazon Web Services - EventBridge"
!constant ICON_AWS_EVENT_BRIDGE_EVENT "Amazon Web Services - EventBridge Event"
!constant ICON_AWS_EVENT_BRIDGE_RULE "Amazon Web Services - EventBridge Rule"
!constant ICON_AWS_EVENT_BRIDGE_CUSTOM_EVENT_BUS "Amazon Web Services - EventBridge Custom Event Bus"
!constant ICON_AWS_COGNITO "Amazon Web Services - Cognito"
!constant ICON_KOTLIN "Kubernetes - pod"
!constant ICON_KOTLIN_COMPONENT "Google Cloud Platform - Cloud Functions"