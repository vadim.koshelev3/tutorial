# Externeal Software Systems
!include ../softwareSystems/core/productService/productService.dsl
!include ../softwareSystems/core/inventoryService/inventoryService.dsl
!include ../softwareSystems/core/salesService/salesService.dsl
!include ../softwareSystems/core/commercialService/commercialService.dsl
!include ../softwareSystems/core/flightService/flightService.dsl
!include ../softwareSystems/core/dynamicPanda/dynamicPanda.dsl

# PandA Wholesale Software Systems Group
pandaService = enterprise "${GROUP_PANDA_WHOLESALE_SERVICE}" {            
    # PandA Wholesale - Software Systems
    !include ../softwareSystems/core/ingestionApp/ingestionApp.dsl
    !include ../softwareSystems/core/eventApp/eventApp.dsl
    !include ../softwareSystems/core/apiApp/apiApp.dsl
}
#
# Persons
#
tuiCustomer = person "TUI Customer"{
    tags "TUI Customer"
}
#
# Data Flow Relationships: Software Systems 
#
# Flight Service 
flightService -> ingestionApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
}
flightService.journeyInventoryChangeEventsSNS -> ingestionApp.journeyInventoryChangeEventsIngestionQueue ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
}
# Inventory Service 
inventoryService -> ingestionApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
}
inventoryService.inventoryTransactionCreatedEventsSNS -> ingestionApp.inventoryTransctionCreatedEventsIngestionQueue ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
}  
# Product Service
productService -> ingestionApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}   
productService.productUnitsKDS -> ingestionApp.productEventsIngestionKinesisApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
productService.productComponentsKDS -> ingestionApp.productEventsIngestionKinesisApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
productService.productsKDS -> ingestionApp.productEventsIngestionKinesisApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
# Commercial Service
commercialService -> ingestionApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
# Ingestion App
ingestionApp -> apiApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
ingestionApp -> eventApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
# API App
apiApp -> eventApp ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
apiApp.apiGateway -> salesService "Package/MMB/StayOnly data"{
    tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
}
apiApp.auth -> salesService "Auth token"{ 
    tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
}

# Event App
eventApp -> commercialService ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
eventApp -> salesService ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}
# Sales Service
salesService -> tuiCustomer ""{
    tags "${TAG_RELATIONSHIP_DATA_FLOW}"
}

#
# Sales Relationships: Software Systems and Containers
#
salesService -> apiApp.auth  "Authentication Credentials"{
    tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
}

#
# Dynamic Panda Relationships
#
dynamicPanda -> apiApp.pandaCacheApp.stayV3  "Dynamic Stay Data"{
    tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
}
