!include "../softwareSystems/other/productDomain/productDomain.dsl"
!include "../softwareSystems/other/commercialDomain/commercialDomain.dsl"
!include "../softwareSystems/other/inventoryDomain/inventoryDomain.dsl"
!include "../softwareSystems/other/commonData/commonData.dsl"

productDomain -> commercialDomain {
    tags "${TAG_DATA_FLOW}"
}
productDomain -> inventoryDomain {
    tags "${TAG_DATA_FLOW}"
}
productDomain -> commonData {
    tags "${TAG_DATA_FLOW}"
}