!include constants/core.dsl

workspace {
    name "E2E Arcitecture: With Domains" 
    !identifiers hierarchical
    model {        
        
        !include "models/core.dsl"
        
        group "Domain" {
            !include "models/domain.dsl"
        }

        productService -> productDomain "Domain Reference" {
            tags "${TAG_DATA_FLOW}"
        }
        
        commercialService -> commercialDomain "Domain Reference" {
            tags "${TAG_DATA_FLOW}"
        }
    }
    views {

        # systemlandscape "SystemLandscapeDataflow" {
        #     title "${GROUP_PANDA_WHOLESALE_SERVICE}"
        #     include *
        #     # exclude "tag==${TAG_DOMAIN}"
        #     filtered "landscape" exclude "Domian"

        #     autoLayout lr 300 300
        #     # !script groovy {
        #     #     workspace.views.createDefaultViews()
        #     #     workspace.views.views.forEach { }
        #     # }
        # }

        !include "views/styles.dsl"
        !include "views/themes.dsl"
    }
}
