commercialDomain = softwareSystem "Commercial Domain" {
    tags "${TAG_DOMAIN}"
    saleableUnit = container Unit {
        description "Saleable Unit Aggregate"
        component AccommodationPriceSet {
        }
    }
}