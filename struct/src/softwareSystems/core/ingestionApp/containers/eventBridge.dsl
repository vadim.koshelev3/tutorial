eventBridge = container "EventBridge" {
    technology "EventBridge"
    tags "${ICON_AWS_EVENT_BRIDGE},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"

    group_inventoryTransctionCreatedEventsEvents = group "Inventory Transaction Created Events" {
        # Components
        inventoryTransctionCreatedEventsArchive = component "Inventory Transaction Created: Event Archive" {
            tags "${ICON_AWS_EVENT_BRIDGE_EVENT},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
        } 
        inventoryTransctionCreatedEventsRule = component "Inventory Transaction Created: Event Rules" {
            tags "${ICON_AWS_EVENT_BRIDGE_RULE},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
        }  
        inventoryTransctionCreatedEventsBus = component "Inventory Transaction Created: Event Bus" {
            tags "${ICON_AWS_EVENT_BRIDGE_CUSTOM_EVENT_BUS},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
        }
        # Relationships
        inventoryTransctionCreatedEventsBus -> inventoryTransctionCreatedEventsArchive "" {
            tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
        }
        inventoryTransctionCreatedEventsBus -> inventoryTransctionCreatedEventsRule "" {
            tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
        }
    }

    group_productEvents = group "Product Events" {
        # Components
        journeyInventoryChangeEventsEventBus = component "Journey Inventory Change Events: Event Bus" {
            tags "${ICON_AWS_EVENT_BRIDGE_CUSTOM_EVENT_BUS},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
        }
        journeyInventoryChangeEventsRule = component "Journey Inventory Change Events: Event Rules" {
            tags "${ICON_AWS_EVENT_BRIDGE_CUSTOM_EVENT_BUS},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
        } 
        journeyInventoryChangeEventsArchive = component "Journey Inventory Change Events: Event Archive" {
            tags "${ICON_AWS_EVENT_BRIDGE_EVENT},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
        }
        # Relationships
        journeyInventoryChangeEventsEventBus -> journeyInventoryChangeEventsRule "" {
            tags "${TAG_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
        }
        journeyInventoryChangeEventsEventBus -> journeyInventoryChangeEventsArchive "" {
            tags "${TAG_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
        }        
    }


    # eventBridgeJourneyInventoryChangeGroup = group "Journey Inventory Change  Events" {
    #     journeyInventoryChangeEBEventBus  = component "Journey Inventory Change Event Bus" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_CUSTOM_EVENT_BUS}"
    #     }

    #     journeyInventoryChangeEBRule = component "Journey Invenotry Change Rules" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_RULE}"
    #     }  

    #     journeyInventoryChangeEBArchive = component "Journey Invenotry Change Archive" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_EVENT}"
    #     }   
    # }
    # eventBridgeBasePriceGroup = group "Base Price  Events" {
    #     basePriceEBArchive = component "Base Price Archive" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_EVENT},${TAG_COMMERCIAL_SERVICE},${TAG_ACCOM_PRICE_SET_CREATED},${TAG_ACCOM_PRICE_SET_CHANGED},${TAG_TRANSPORT_PRICE_SET_CREATED},${TAG_TRANSPORT_PRICE_SET_CHANGED}"
    #     } 
    #     basePriceEBRule = component "Base Price Rules" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_RULE},${TAG_COMMERCIAL_SERVICE},${TAG_ACCOM_PRICE_SET_CREATED},${TAG_ACCOM_PRICE_SET_CHANGED},${TAG_TRANSPORT_PRICE_SET_CREATED},${TAG_TRANSPORT_PRICE_SET_CHANGED}"
    #     }
    #     basePriceEBEventBus  = component "Base Price Event Bus" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_CUSTOM_EVENT_BUS},${TAG_COMMERCIAL_SERVICE},${TAG_ACCOM_PRICE_SET_CREATED},${TAG_ACCOM_PRICE_SET_CHANGED},${TAG_TRANSPORT_PRICE_SET_CREATED},${TAG_TRANSPORT_PRICE_SET_CHANGED}"
    #     }
    # }   
    # eventBridgeBasePriceYieldAdjustmentGroup = group "Base Price Yield Adjustment Events" {
    #     basePriceYieldAdjustmentEBArchive = component "Base Price Yield Adjustment Archive" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_EVENT},${TAG_COMMERCIAL_SERVICE},${TAG_YIELD_FLIGHT_ACCOMMODATION_FLIGHT}"
    #     }        
    #     basePriceYieldAdjustmentEBRule = component "Base Price Yield Adjustment Rules" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_RULE},${TAG_COMMERCIAL_SERVICE},${TAG_YIELD_FLIGHT_ACCOMMODATION_FLIGHT}"
    #     }                                                   
    #     basePriceYieldAdjustmentEBEventBus  = component "Base Price Yield Adjustment Event Bus" {
    #         tags "${ICON_AWS_EVENT_BRIDGE_CUSTOM_EVENT_BUS},${TAG_COMMERCIAL_SERVICE},${TAG_YIELD_FLIGHT_ACCOMMODATION_FLIGHT}"
    #     }
    # }
}

