ingestionApp = softwareSystem "Ingestion APP" {
    tags "${TAG_JOURNEY_INVENTORY_CHANGE_EVENT},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"

    #
    # Containers
    #

    # Include all containers from the containers folder
    !include "containers/"

    # Journey Inventory Change Events #
    journeyInventoryChangeEventsIngestionQueue = container "Journey Inventory Change Events: Ingestion Queue" {
        technology "SQS"
        tags "${ICON_AWS_SQS},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
    }
    journeyInventoryChangeEventsEventbridgeInjector = container "Journey Inventory Change Events: EventBridge Injector" {
        technology "Lambda"
        tags "${ICON_AWS_LAMBDA},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
        properties {
            AWSResourceName  wr-<env>-sqs-engines-journey-inventory-events
            Repository https://source.tui/price-and-availability-service/application/engines-apps
            RepositoryKeyword engines-journey-inventory-events
        }
    }
    journeyInventoryChangeEventsValidEventsQueue = container "Journey Inventory Change Events: Valid Events Queue" {
        technology "TypeScript"
        description ""
        tags "${ICON_AWS_LAMBDA},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
    }
    # Inventory Transaction Created Events #
    inventoryTransctionCreatedEventsIngestionQueue = container "Inventory Transaction Created Events: Ingestion Queue" {
        technology "AWS SQS"
        description ""
        tags "${ICON_AWS_SQS},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
    }
    inventoryTransctionCreatedEventsEventbridgeInjector = container "Inventory Transaction Created Events: EventBridge Injector" {
        technology "TypeScript"
        description ""
        tags "${ICON_AWS_LAMBDA},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
    }
    inventoryTransctionCreatedValidEventsQueue = container "Inventory Transaction Created: Valid Events Queue" {
        technology "AWS SQS"
        description ""
        tags "${ICON_AWS_SQS},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
    }
    inventoryTransctionCreatedValidEventsSplitter = container "Inventory Transaction Created: Valid Events Splitter" {
        technology "AWS Lambda"
        description ""
        tags "${ICON_AWS_LAMBDA},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"
    }
    inventoryTransctionCreatedValidSplitEventsQueue = container "Inventory Transaction Created: Valid Split Events Queue" {
        technology "AWS Lambda"
        description ""
        tags "${ICON_AWS_SQS},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"
    }

    # Product Events #
    productEventsIngestionKinesisApp = container "Product Events: Kinesis App" {
        technology "Kotlin"
        description ""
        tags "${ICON_AWS_KINESIS}"
    }
    productEventsIngestionQueue = container "Product Events: Ingestion Queue" {
        technology "AWS SQS"
        description ""
        tags "${ICON_AWS_SQS}"
    }
    productEventsIngestionQueueValidEventsQueue = container "Product Events: Valid Events Queue" {
        technology "AWS Lambda"
        description ""
        tags "${ICON_AWS_SQS}"
    }
    #
    # Relationships
    #

    # Journey Inventory Change Events Events #
    journeyInventoryChangeEventsIngestionQueue -> journeyInventoryChangeEventsEventbridgeInjector "" {
        tags "${TAG_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
    }
    #[
        journeyInventoryChangeEventsEventbridgeInjector -> eventBridge "" {
            tags "${TAG_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
        }
        journeyInventoryChangeEventsEventbridgeInjector -> eventBridge.journeyInventoryChangeEventsEventBus "" {
            tags "${TAG_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
        }
    #]
    eventBridge.journeyInventoryChangeEventsRule -> journeyInventoryChangeEventsValidEventsQueue "" {
        tags "${TAG_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
    }
    journeyInventoryChangeEventsValidEventsQueue -> commandApp "" {
        tags "${TAG_DATA_FLOW},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
    }
    # Invectory Transaction Created Events #
    inventoryTransctionCreatedEventsIngestionQueue -> inventoryTransctionCreatedEventsEventbridgeInjector "" {
        tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
    }
    #[
        inventoryTransctionCreatedEventsEventbridgeInjector -> eventBridge "" {
            tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
        }
        inventoryTransctionCreatedEventsEventbridgeInjector -> eventBridge.inventoryTransctionCreatedEventsBus "" {
            tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
        }
    #]

    eventBridge.inventoryTransctionCreatedEventsRule -> inventoryTransctionCreatedValidEventsQueue "" {
        tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
    }
    inventoryTransctionCreatedValidEventsQueue -> inventoryTransctionCreatedValidEventsSplitter "" {
        tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
    }
    inventoryTransctionCreatedValidEventsSplitter -> inventoryTransctionCreatedValidSplitEventsQueue "" {
        tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"
    }
    #[
        inventoryTransctionCreatedValidSplitEventsQueue -> commandApp "" {
            tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"
        }
        inventoryTransctionCreatedValidSplitEventsQueue -> commandApp.eventListener "" {
            tags "${TAG_DATA_FLOW},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"
        }
    #]


    # Product Events #
    productEventsIngestionKinesisApp -> productEventsIngestionQueue "" {
        tags "${TAG_DATA_FLOW}"
    }    
    productEventsIngestionQueue -> eventBridge "" {
        tags "${TAG_DATA_FLOW}"
    }


    #
    # Relationships: Container to Component Data Flow 
    #
    commandApp.commandHandlerInventoryTransactionCreatedCommand -> commandAppDatabase "" {
        tags "${TAG_DATA_FLOW}"
    }
    commandApp.commandHandlerJourneyInventoryChangeCommand -> commandAppDatabase "" {
        tags "${TAG_DATA_FLOW}"
    }

}