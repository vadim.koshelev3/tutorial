inventoryService = softwareSystem "Inventory Service" {
    tags "${TAG_EXTERNAL_SERVICE},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"

    inventoryTransactionCreatedEventsSNS = container "Inventory Transaction Created: SNS" {
        tags "${ICON_AWS_SNS},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"

        inventoryTransactionCreatedSnsEvent = component "InventoryTransactionCreated" {
            tags "${ICON_AWS_SNS},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT}"
            url https://confluence.tuigroup.com/x/bcmtNg
        }

    }    

}
