apiApp = softwareSystem "API APP" {
    tags ""

    #
    # Containers
    #

    # Include all containers from the containers folder
    #!include "containers/"

    apiGateway = container "Api Gateway" {
        technology "AWS Api Gateway"
        tags "${ICON_AWS_API_GATEWAY}"
        cmd = component "/cmd" "Api Gateway Resource" "" {}
        mmb = component "/mmb" "Api Gateway Resource" "" {}
        pa = component "/pa" "Api Gateway Resource" "" {}
        panda = component "/panda" "Api Gateway Resource" "" {}
        qp = component "/qp" "Api Gateway Resource" "" {}

    }

    auth = container "Authentication" {
        technology "AWS Congnito"
        tags "${ICON_AWS_COGNITO}" 
    }

    mmbApp = container "MMB App" {
        technology "Kotlin/SpringBoot/WebFlux"
        tags "${ICON_KOTLIN}" 
        uniqueOffer = component "/v1/packages/{product-id}/{accommodation-code}/{start-date}" "Unique offer Api Endpoint" "" {}
        alternativeRoom = component "/v1/packages/{product-id}/{accommodation-code}/{start-date}/alternatives/room" "Alternatives Room Api Endpoint" "" {}
    }
    pandaCacheApp = container "PandaCache App" {
        technology "Kotlin/SpringBoot/WebFlux"
        tags "${ICON_KOTLIN}" 
        stayV3 = component "/stays/v3/*" "Stay V3 Api Endpoints" "" {}
    }

    pandaCacheRedis = container "Panda Cache Redis" {
        technology "Redis"
        tags "${ICON_AWS_ELASTIC_CACHE}" 
    }

    queryApp = container "Query APP" {
        technology "Kotlin/SpringBoot/WebFlux"
        tags "${ICON_KOTLIN}"
        stayV2 = component "/stays/v2/*" "Stay V2 Api Endpoints" "" {}
        stayV3 = component "/stays/v3/*" "Stay V3 Api Endpoints" "" {}
        packageV2 = component "/packages/v2/*" "Package V2 Api Endpoints" "" {}
        packageV3 = component "/packages/v3/*" "Package V3 Api Endpoints" "" {}
        otaG7V1 = component "/ota-g7/v1/*" "OTA G7 V1 Api Endpoints" "" {}
    }

    queryDB = container "Query DB" {
        technology "AWS RDS"
        tags "${ICON_AWS_RDS}" 
    }

    queryProxy = container "Query Proxy" {
        technology "Kotlin/SpringBoot/WebFlux"
        tags "${ICON_KOTLIN}" 
        stayV2 = component "/stays/v2/*" "Stay V2 Api Endpoints" "" {}
        stayV3 = component "/stays/v3/*" "Stay V3 Api Endpoints" "" {}
        packageV2 = component "/packages/v2/*" "Package V2 Api Endpoints" "" {}
        packageV3 = component "/packages/v3/*" "Package V2 Api Endpoints" "" {}
        otaG7V1 = component "/ota-g7/v1/*" "OTA G7 V1 Api Endpoints" "" {}
    }

    yieldApp = container "Yield App" {
        technology "Kotlin/SpringBoot/WebFlux"
        tags "${ICON_KOTLIN}" 
        packageV3 = component "package/bulk" "Package V3 Yield Endpoint" "" {}
        packageV2 = component "v2/package/bulk" "Package V2 Yield Endpoint" "" {}
        
    }

    yieldRedis = container "Yield Redis" {
        technology "Redis"
        tags "${ICON_AWS_ELASTIC_CACHE}" 
    }
    

    #
    # Api Gateway Relationships 
    #
    auth -> apiGateway  "User Identity"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryProxy -> apiGateway.qp  "Package/Ota G71 Response"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryProxy.packageV3 -> apiGateway.qp  "Package Response"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    queryProxy.packageV2 -> apiGateway.qp  "Package Response V2"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryProxy.stayV3 -> apiGateway.qp  "Stay Only Response V2"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryProxy.otaG7V1 -> apiGateway.qp  "OTA G7 Response"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.stayV2 -> apiGateway.pa  "Query App Stay Response"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.stayV3 -> apiGateway.pa  "Query App Stay Response"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.packageV2 -> apiGateway.pa  "Query App Package Response"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.packageV3 -> apiGateway.pa  "Query App Package Response"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.otaG7V1 -> apiGateway.pa  "Query App OTA G7 Response"{ 
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    mmbApp -> apiGateway.mmb  "MMB Response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    mmbApp.uniqueOffer -> apiGateway.mmb  "MMB Unique Offer"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    mmbApp.alternativeRoom -> apiGateway.mmb  "MMB Alternate Room"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    
    pandaCacheApp.stayV3 -> apiGateway.panda  "Stay Only Response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    
    #
    # queryProxy Relationships 
    #
    queryApp -> queryProxy  "Sliced response/Reverse Proxy"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.stayV3 -> queryProxy.stayV3  "Proxied/Sliced Stay response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.packageV3 -> queryProxy.packageV3  "Proxied/Sliced Package response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.packageV2 -> queryProxy.packageV2  "Proxied/Sliced Package V2 response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.stayV3 -> queryProxy.stayV3  "Proxied/Sliced Stay V2 response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryApp.otaG7V1 -> queryProxy.otaG7V1  "Proxied/Sliced OTA G7 response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    #
    # queryApp Relationships 
    #
    yieldApp.packageV2 -> queryApp.packageV2  "Calculated yields"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    yieldApp.packageV3 -> queryApp.packageV3  "Calculated yields"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    
    queryDB -> queryApp  "Package Data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC},${ICON_AWS_RDS}"
    }

    queryDB -> queryApp.stayV2  "Stay V2 Data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryDB -> queryApp.stayV3  "Stay V3 Data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    
    queryDB -> queryApp.packageV2  "Package V2 Data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryDB -> queryApp.packageV3  "Package V3 Data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    
    queryDB -> queryApp.otaG7V1  "OTA G7 V1 Data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    #
    # yieldApp Relationships 
    #
    yieldApp.packageV2 -> yieldRedis  "Cache result"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_ASYNC}"
    }

    yieldApp.packageV3 -> yieldRedis  "Cache result"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_ASYNC}"
    }
    
    yieldRedis -> yieldApp.packageV2  "Extract Cached Results"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    yieldRedis -> yieldApp.packageV3  "Extract Cached Results"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    queryDB -> yieldApp.packageV3  "Yield data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    queryDB -> yieldApp.packageV2  "Yield data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }


    #
    # pandaCacheApp Relationships
    #

    queryProxy.stayV3 -> pandaCacheApp.stayV3  "Whole sale stay Only Data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    pandaCacheApp.stayV3 -> pandaCacheRedis  "Add offers data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_ASYNC}"
    }

    pandaCacheRedis -> pandaCacheApp.stayV3  "Extract Offer data"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

    #
    # mmbApp Relationships
    #
    queryApp.packageV3 -> mmbApp "Unique offer/Alternate room"{
            tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    queryApp.packageV3 -> mmbApp.uniqueOffer "Unique offer Response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }
    queryApp.packageV3 -> mmbApp.alternativeRoom "Alternate room response"{
        tags "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}"
    }

}