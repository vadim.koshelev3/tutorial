    productsExportKDS = container "Products" "" "Kinesis Data Stream" {
        tags "Amazon Web Services - Kinesis Data Streams"

        -> priceAndAvailabilityIngestionApp "" "" ""

        productCreated = component "PRODUCT_CREATED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productChanged = component "PRODUCT_CHANGED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productRetired = component "PRODUCT_RETIRED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
    } 