productService = softwareSystem "Product Service" {
    tags "${TAG_EXTERNAL_SERVICE}"

   productsKDS = container "Products" "" "Kinesis Data Stream" {
        tags "Amazon Web Services - Kinesis Data Streams"

        productCreated = component "PRODUCT_CREATED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productChanged = component "PRODUCT_CHANGED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productRetired = component "PRODUCT_RETIRED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
    }

    productUnitsKDS = container "Product Units" {
        tags "Amazon Web Services - Kinesis Data Streams,"
        
        productAccommodationUnitCreated = component "PRODUCT_ACCOMMODATION_UNIT_CREATED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productAccommodationUnitChanged = component "PRODUCT_ACCOMMODATION_UNIT_CHANGED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productAccommodationUnitRetired = component "PRODUCT_ACCOMMODATION_UNIT_RETIRED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productFlightUnitCreated = component "PRODUCT_FLIGHT_UNIT_CREATED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productFlightUnitChanged = component "PRODUCT_FLIGHT_UNIT_CHANGED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productFlightUnitRetired = component "PRODUCT_FLIGHT_UNIT_RETIRED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
    }                  
    productComponentsKDS = container "Product Components"{
        tags "Amazon Web Services - Kinesis Data Streams,"

        productAccommodationComponentCreated = component "PRODUCT_ACCOMMODATION_COMPONENT_CREATED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productAccommodationComponentChanged = component "PRODUCT_ACCOMMODATION_COMPONENT_CHANGED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productAccommodationComponentRetired = component "PRODUCT_ACCOMMODATION_COMPONENT_RETIRED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productFlightComponentCreated = component "PRODUCT_FLIGHT_COMPONENT_CREATED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productFlightComponentChanged = component "PRODUCT_FLIGHT_COMPONENT_CHANGED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
        productFlightComponentRetired = component "PRODUCT_FLIGHT_COMPONENT_RETIRED" "Event" {
            url https://confluence.tuigroup.com/x/ba8KOQ
        }
    } 

}
