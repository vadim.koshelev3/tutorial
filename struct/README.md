
# Getting started

## Local development

### Using Structurizer Lite
Follow the guid below our look at the offical guide:
	https://structurizr.com/help/lite/getting-started

#### Clone the repository to your local computer
	git clone git@ssh.source.tui:price-and-availability-service/architecture/architecture-c4-model.git
Note down the path to the folder that contains the repository
#### Start structurizr/lite docker container locally on your computer
	docker run -it --rm -p 8081:8080 -v localFolder:/usr/local/structurizr structurizr/lite
Replace localFolder with the path to the folder that contains your repository.

Linux example:

	docker run -it --rm -p 8081:8080 -v ~/Projects/architecture-c4-model:/usr/local/structurizr structurizr/lite
Windows example:

	docker run -it --rm -p 8081:8080 -v c:/my_space\projects\tui\architecture-c4-model:/usr/local/structurizr structurizr/lite
8081 is the port that is used locally to open the structurizer/lite tool
#### Open your favorite browser
And enter the following URL
		
	http://localhost:8081/
		
When you make a change make to the DSL model make sure you reload the browser
#### Pick workspace to display in the browser
Open the workspace.dsl file in the root folder of the repository.

    workspace extends src/core.dsl {
	    name "Workspace for DSL Lite compatability"
	}
Replace core.dsl with the workspace you want to display in the browser

Example: 	

    workspace extends src/domain.dsl {
	    name "Workspace for DSL Lite compatability"
	}
